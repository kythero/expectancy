# Expectancy

I met a difficulty during execution some functions which should execute in different time.
My main project has a single method **update()** (similar like in GUIs or pygame rendering) and inside there 
I have few functions with different jobs to do. Not every function should execute in the same time.
Ones can execute every 15 seconds, others every 23 seconds and so on.

Expectancy is something like a handler to keep those declared functions with their arguments and timer (interval) 
and execute them after the interval pass.

When I register my functions inside the Expectancy, now I could run in my single update method,
just Expectancy.execute_all(), and each function has its own timer, which allow me handle my issue.
Now some functions execute in every 5 seconds, others every 7 seconds and so on! and it will not affect my core script, 
which has its own logic.

How to register? It's simple. Import Expectancy and use its classmethods.
In example I register same function two times. If you want to register single function more than one time
you need to specify it's name, like it is done in second registration. Otherwise, you will get an exception **RegisterError**.

```python
from expectancy import Expectancy
from datetime import datetime
import time


def dummy_func(word):
    """
    Our function will print "word" in some time.
    """
    print("%s - %s" % (datetime.now().strftime("%H:%M:%S"), word))

if __name__ == '__main__':
    Expectancy.register(func=dummy_func, args=('Hello world',), interval=3)
    Expectancy.register(func=dummy_func, args=('Hello Mike!',), name="other", interval=1)
    while True:
        Expectancy.execute_all()
        time.sleep(1)
```

and results:
```python
21:16:24 - Hello Mike!
21:16:25 - Hello Mike!
21:16:26 - Hello world
21:16:26 - Hello Mike!
21:16:27 - Hello Mike!
21:16:28 - Hello Mike!
21:16:29 - Hello world
21:16:29 - Hello Mike!
```

Expectancy use only python **datetime** and **time** library to calculate when execute the registered function.
The **Expectancy.execute_all()** have to be implemented in a loop.

## Expectancy and its methods.

Main methods are **Expectancy.execute_all()** and **Expectancy.execute(*func_name*)** where you need to specify, which func should be executed.
When you use Expectancy.register() as a return you get Register object, which handle timers.
The Register object has method **rouse()** which will invoke immediately the registered function and reset its timer.

## Simple object: method wait(callable)

use do(func).wait(callable) if you want to check some condition after do execute.

Example: 
I would like to follow my character, but first I need target it. Execute only target does not tell me 
if the target is already done, so I need some additional condition.

A bot/client will have a time to 'think'. In some time once per milion we will have lag, 
and bot will need to think ~2 seconds to do an action, but in another time, action will take only ~0.3 sec.
This solution with timeouts will take care of it.

```python
from Expectancy.expectancy import Expectancy

def do_target():
    # execute command, target
    target("nick")

def do_target_reason():
    """
    :return: If valid, return True, False otherwise.
    """
    validate = False
    if targeted_hp == guardian_hp_max:
        validate = True
    return validate

if __name__ == '__main__':
    try:
        # execute func inside do() once and wait 4 seconds for condition (do_target_reason).
        Expectancy.do(func=do_target).wait(func=do_target_reason, timeout=4)
        print("TARGETED PROPERLY!")
    except Expectancy.core.exceptions.TimeoutError as ex:
        print("Did not target! Exception: {}".format(ex))

```

## Expectancy and workers.

When you run **Expectancy.execute_all()**, the method uses ThreadPoolExecutor. Default number of workers is **2**. 
If you want to change it, only what you need to do is create environment variable EXPECTANCY_MAX_WORKERS before you run script.
In example, I set number of workers to **5**.
```bash
    #!/bin/bash
    export EXPECTANCY_MAX_WORKERS=5
```

```tcsh
    #!/bin/tcsh
    setenv EXPECTANCY_MAX_WORKERS 5
```

## Beta Simple object

Simple object can help you when you want to execute some action few times until something another happens.
Its like "polling". Simple object can be build from **Expectancy.do(*func*)**
In example below, I created dummy methods, where method **until()** returns only False. 
Simple object, **do(*func*)** until callable object (which is dummy_func_until) returns True.
It will never happen, thats why we except TimeoutError.

```python
from expectancy import Expectancy
from core.exceptions import TimeoutError
from datetime import datetime
import time

def dummy_func(word):
    """
    Our function will print "word" in some time.
    """
    print("%s - %s" % (datetime.now().strftime("%H:%M:%S"), word))

def dummy_func_until():
    time.sleep(1)
    return False

if __name__ == '__main__':
    print("%s - %s" % (datetime.now().strftime("%H:%M:%S"), "started"))
    try:
        Expectancy.do(func=dummy_func, args=("Hello world",)).until(dummy_func_until, timeout=5)
    except TimeoutError:
        print("Catched error")
    print("%s - %s" % (datetime.now().strftime("%H:%M:%S"), "finished"))

```

and results:
```python
21:39:27 - started
21:39:27 - Hello world
21:39:29 - Hello world
21:39:31 - Hello world
21:39:33 - Hello world
21:39:35 - Hello world
Catched error
21:39:37 - finished
```

Simple object uses time.sleep(1) where "1" is a second.
 
## Warning
patch: 1.2.0: Simple object got new method: **wait(** *func=callable, timeout=3* **)**

patch: 1.1.0: Expectancy got *concurrent.futures.ThreadpoolExecutor* for **Expectancy.execute_all()**.

patch: 1.0.0: To proper use, do not register functions with sleep inside them. Expectancy do not use threads/processes. 
Registered functions with sleeps, may affect whole script.


## How to use

Clone my repository and have fun!

```
git clone https://github.com/marekgancarz/expectancy.git
```

## Changes

**patch: 1.1.2**
* *Simple* object got method **only_if(*expression*, *timeout*)**, available from **Expectancy.do(*func*, *args*)**.
* **EXPECTANCY_MAX_WORKERS** can be set from system environment variables.

**Patch: 1.1.0**
* Now you can unregister saved function with **Expectancy.unregister(*element*)** 
where element is Register object, func or declared name.
* **Expectancy.execute_all()** uses *concurrent.futures.ThreadPoolExecutor*. 
Beta tests allows using sleeps, in registered functions. (still testing - be aware.)

## Features

* Usable during GUIs, when you want to execute small pieces of code in some intervals. (cleaners for example)
* During pygame, when you want to execute some cleaners during main update/render method.
* usable during writing bots. When bot should do something every 6 seconds.
    Many bot's actions can be registered. 
* allow register methods and invoke them betweenwhiles.
* Is awesome

