"""
Exceptions for expectancy module.
"""


class RegisterError(Exception):
    """
    Raise the RegisterError Exception when you cannot register your Function.
    """


class TimeoutError(Exception):
    """
    Raise the TimeoutError Exception when your awaiting methods times out.
    """


class NameError(Exception):
    """
    Raise the NameError Exception when your name function cannot be found or it is wrong.
    """
