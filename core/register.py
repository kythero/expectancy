from datetime import datetime, timedelta
from .simple import Simple


class Register(Simple):

    def __init__(self, func, arguments, interval):
        super(Register, self).__init__(func, arguments)
        self._execute_time = datetime.now()
        self._interval = self._validate_interval(interval)

    @classmethod
    def _validate_interval(cls, new_interval):
        if isinstance(new_interval, int) or isinstance(new_interval, float):
            return new_interval
        else:
            raise ValueError("Interval must be a int or float. Units are seconds.")

    def execute(self):
        """
        Main method. If interval time pass, it will execute registered function with its arguments.

        :return: func's response, False otherwise.
        """
        if self._execute_time + timedelta(seconds=self._interval) < datetime.now():
            self._execute_time = datetime.now()
            return self._func(*self._args)
        return False

    def rouse(self):
        """
        Rouse registered function immediately and reset its timer.

        :return: func's response.
        """
        self._execute_time = datetime.now()
        return self._func(*self._args)

    def get_registered_function(self):
        """
        Returns registered function.

        :return: function.
        """
        return self._func
