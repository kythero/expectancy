from .exceptions import TimeoutError
from datetime import datetime, timedelta
import time


class Simple(object):

    UNTIL_DELAY = 1  # second
    TIMEOUT_LOSE = 1  # second

    def __init__(self, func, args=()):
        self._created = datetime.now()
        self._func = func
        self._args = args

    def until(self, expression, timeout=10):
        """
        Execution in every second given function in Await object, until expression finish or timeout occur.
        Warning: It uses time.sleep(). It will stuck your main script.

        :param expression: simple expression or callable object
        :param timeout: default 10 seconds.
        :exception: TimeoutError
        :return: func response given in Await object.
        """
        while timeout > 0:
            response = self._func(*self._args)
            if callable(expression):
                result = expression()
            else:
                result = expression
            if result:
                return response

            timeout -= self.TIMEOUT_LOSE
            time.sleep(self.UNTIL_DELAY)

        raise TimeoutError("Timeout until() method for given function: {} with args: '{}'.".format(self._func.__name__, self._args))

    def wait(self, func=None, args=(), timeout=3):
        """
        Execute func, wait for a given period of time (timeout) and

        :param func: callable object
        :param args: callable object's args
        :param timeout: default 3 seconds
        :return: saved func's result if success, otherwise raise timeout.
        """
        if not callable(func):
            raise TypeError("Expression must be a callable object.")
        response = self._func(*self._args)
        final_time = datetime.now() + timedelta(seconds=timeout)
        while final_time > datetime.now():
            result = func(*args)
            if result:
                return response

        raise TimeoutError("Timeout wait() method for given function: {} with args: '{}'.".format(self._func.__name__, self._args))

    def only_if(self, expression, timeout=10):
        """
        Execute func only if expression pass.

        :param expression: callable object or simple boolean.
        :param timeout: default 10 seconds.
        :return: func response.
        """
        while timeout > 0:

            if callable(expression):
                result = expression()
            else:
                result = expression

            if result:
                return self._func(*self._args)

            timeout -= self.TIMEOUT_LOSE
            time.sleep(self.UNTIL_DELAY)

        raise TimeoutError("Timeout")

    @property
    def created(self):
        return self._created
