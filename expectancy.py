from .core.register import Register
from .core.exceptions import RegisterError
from .core.simple import Simple
from concurrent.futures import ThreadPoolExecutor
import logging
import os

LOGGER = logging.getLogger(__name__)


class Expectancy(object):

    registered = {}
    MAX_WORKERS = int(os.environ.get('EXPECTANCY_MAX_WORKERS', 2))

    @classmethod
    def register(cls, func=None, args=(), interval=5, name=None):
        """
        Register your function in Expectancy.
        
        :param func: function to register
        :param args: arguments for given function
        :param interval: interval for executing this function, default 5 seconds.
        :param name: default None. Your function's name to be registered.
        :exception: RegisterError raise if already name for function exists.
        :return: Register object, the registered function.
        """
        func_name = cls._get_name(func=func, name=name)
        is_available = cls.registered.get(func_name)
        if is_available:
            raise RegisterError("There is already registered function with given name %s" % (func_name,))
        else:
            cls.registered[func_name] = Register(func, args, interval)
            return cls.registered[func_name]

    @classmethod
    def unregister(cls, registered):
        """
        Unregister saved function.

        :param registered: it can be Register object, func or declared name.
        :return: True if unregistered property. False otherwise.
        """
        func_name = None
        if isinstance(registered, Register):
            for key, value in cls.registered.items():
                if registered == value:
                    func_name = key
                    break
        elif callable(registered):
            func_name = registered.__name__.lower()
        elif isinstance(registered, str):
            func_name = registered.lower()
        else:
            LOGGER.debug("Could not find registered function.")

        try:
            if func_name:
                del cls.registered[func_name]
                return True
        except KeyError:
            LOGGER.debug("Could not find given func_name [%s] in registered dict.", func_name)
        return False

    @classmethod
    def rouse(cls, func=None, name=None):
        """
        Rouse registered function.

        :param func: func
        :param name: func's name
        :return: True if rouse properly. False otherwise.
        """
        func_name = cls._get_name(func=func, name=name)
        func = cls.registered.get(func_name)
        if func:
            return func.rouse()
        else:
            raise RegisterError("There is no registered function with given name.")

    @classmethod
    def execute(cls, func=None, name=None):
        """
        Execute method. Main function, it calculates intervals during registration, and execute method if it's ready.

        :param func: func
        :param name: func's name
        :return: func's result, False otherwise.
        """
        func_name = cls._get_name(func=func, name=name)
        func = cls.registered.get(func_name)
        if func:
            return func.execute()
        return False

    @classmethod
    def execute_all(cls):
        """
        Execute method. Execute all registered function, according to their intervals during registration.

        :return: None
        """
        with ThreadPoolExecutor(max_workers=cls.MAX_WORKERS) as executor:
            for _, func in cls.registered.items():
                executor.submit(func.execute)

    @classmethod
    def do(cls, func, args=()):
        """
        Execute given function without registration. It returns Simple object, which allow you use its method.

        :param func: func to execute
        :param args: tuple, func's arguments
        :return: Simple object.
        """
        return Simple(func=func, args=args)

    @classmethod
    def _get_name(cls, func=None, name=None):
        if name:
            _name = name.lower()
        elif func:
            _name = func.__name__.lower()
        else:
            raise NameError("Could not find any func or name.")
        return _name

    @classmethod
    def length(cls):
        """
        Get length of all registered functions

        :return: int
        """
        return len(cls.registered)

    @classmethod
    def clear(cls):
        """
        Remove all registered jobs.

        :return: None
        """
        cls.registered = {}
