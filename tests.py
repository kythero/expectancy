import unittest

from expectancy import Expectancy


class ExpectancyTest(unittest.TestCase):

    def setUp(self):
        self.args = ('hello',)

    @staticmethod
    def func_for_testing(word):
        return word

    def test_register_invalid_interval(self):
        with self.assertRaises(ValueError):
            Expectancy.register(func=self.func_for_testing, args=self.args, interval="no-digit")

    def test_register_correct(self):
        registered = Expectancy.register(func=self.func_for_testing, args=self.args, interval=3)
        self.assertTrue(callable(registered.get_registered_function()))

    def test_register_func_name(self):
        registered = Expectancy.register(func=self.func_for_testing, args=self.args, interval=3)
        response = registered.rouse()
        self.assertEqual("hello", response)

    def test_register_many_functions(self):
        Expectancy.register(func=self.func_for_testing, args=('hello-1',), name="other-1", interval=3)
        Expectancy.register(func=self.func_for_testing, args=('hello-2',), name="other-2", interval=4)
        Expectancy.register(func=self.func_for_testing, args=('hello-3',), name="other-3", interval=7)

        self.assertEqual(Expectancy.length(), 3)

    def test_register_unregister(self):
        registered = Expectancy.register(func=self.func_for_testing, args=('hello-1',), name="other-1", interval=3)
        self.assertTrue(registered)
        unregister = Expectancy.unregister(registered)
        self.assertTrue(unregister)

    def test_register_unregister_name(self):
        registered = Expectancy.register(func=self.func_for_testing, args=('hello-1',), name="other-1", interval=3)
        self.assertTrue(registered)
        unregister = Expectancy.unregister("other-1")
        self.assertTrue(unregister)
        unregister2 = Expectancy.unregister("other-1")
        self.assertFalse(unregister2)

    def test_register_unregister_func(self):
        registered = Expectancy.register(func=self.func_for_testing, args=('hello-1',), interval=3)
        self.assertTrue(registered)
        unregister = Expectancy.unregister(self.func_for_testing)
        self.assertTrue(unregister)
        unregister2 = Expectancy.unregister(self.func_for_testing)
        self.assertFalse(unregister2)

    def tearDown(self):
        Expectancy.clear()


if __name__ == '__main__':
    unittest.main()
